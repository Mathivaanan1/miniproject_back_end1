package com.example.demo.entity;

//import jakarta.annotation.Generated;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name="total")
public class User {

	
	@Id
	@Column(name="id")
	private int id;
	@Column(name="companyname")
	private String companyname;
	@Column(name="price")
	private String price;
	private String date;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCompanyname() {
		return companyname;
	}
	public void setCompanyname(String companyname) {
		this.companyname = companyname;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
//	public User(int id, String companyname, String price) {
//		super();
//		this.id = id;
//		this.companyname = companyname;
//		this.price = price;
//	}
//	
	
	public User(int id, String companyname, String price, String date) {
		super();
		this.id = id;
		this.companyname = companyname;
		this.price = price;
		this.date = date;
	}
	public User() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
}

package com.example.demo.entity;


import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;


@Entity
@Table(name="REGISTER")
public class Model {
	
	
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
     private int id;
	 private String username;
     private String password;
	 private String dob;
	 private String email;
	 private String pan;
     private String totalamount;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPan() {
		return pan;
	}
	public void setPan(String pan) {
		this.pan = pan;
	}
	public String getTotalamount() {
		return totalamount;
	}
	public void setTotalamount(String totalamount) {
		this.totalamount = totalamount;
	}
	public Model(int id, String username, String password, String dob, String email, String pan, String totalamount) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.dob = dob;
		this.email = email;
		this.pan = pan;
		this.totalamount = totalamount;
	}
	public Model() {
		super();
		// TODO Auto-generated constructor stub
	}
	
     
     

}

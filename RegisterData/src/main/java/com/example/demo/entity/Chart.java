package com.example.demo.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name="chart")
public class Chart {
	
	@Id
	@Column(name="id")
	private int id;
	@Column(name="companyname")
	private String companyname;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCompanyname() {
		return companyname;
	}
	public void setCompanyname(String companyname) {
		this.companyname = companyname;
	}
	public Chart(int id, String companyname) {
		super();
		this.id = id;
		this.companyname = companyname;
	}
	public Chart() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

}

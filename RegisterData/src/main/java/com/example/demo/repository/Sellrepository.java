package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Sell;



@Repository
public interface Sellrepository extends JpaRepository<Sell,Integer> {

}

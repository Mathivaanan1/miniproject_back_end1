package com.example.demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Model;
import com.example.demo.entity.Sell;
import com.example.demo.entity.User;
import com.example.demo.service.Sellservice;
import com.example.demo.service.Stackservice;

@CrossOrigin(origins="*")
@RestController
@RequestMapping("/stack")
public class Controls {
	
	@Autowired
	Stackservice ser;
	
	@Autowired
	Sellservice sellservice;
	
	
	@GetMapping("/get")
	
	public List<User> get(){
		return this.ser.getdata();
	}

	@GetMapping("/chart/{companyname}")
	public List<User> get(@PathVariable("companyname") String companyname) {
		
		return this.ser.getone(companyname);
	}
	
	@GetMapping("/sell")
	public List<Sell> getsell(){
		return this.sellservice.getdata();
	}
	@PostMapping("/post")
	public Sell adddata(@RequestBody Sell sell) {
		return this.sellservice.add(sell);
	}
	
	

}

package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Chart;
import com.example.demo.repository.Chartrepository;

@Service
public class Chartservice {
	
	@Autowired
	Chartrepository repository;

	public List<Chart> getdata() {
		// TODO Auto-generated method stub
		return repository.findAll();
	}

	
}

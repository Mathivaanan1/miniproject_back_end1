package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Sell;
import com.example.demo.repository.Sellrepository;

@Service
public class Sellservice {
	
	@Autowired
	Sellrepository sellrepo;

	public List<Sell> getdata() {
		return this.sellrepo.findAll();
		
	}

	public Sell add(Sell sell) {
		// TODO Auto-generated method stub
		return this.sellrepo.save(sell);
	}

}

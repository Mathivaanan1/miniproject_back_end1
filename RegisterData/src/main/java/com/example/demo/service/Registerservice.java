package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import com.example.demo.entity.Model;
//import com.example.demo.entity.User;
import com.example.demo.repository.Registerrepository;


@Service
public class Registerservice {
	
	@Autowired
	Registerrepository repo;

	public Model savedata(Model model) {
		
		return repo.save(model);
	}

	public ResponseEntity<Model> log(Model modeldata) {
		
		Model model=repo.findByUsername(modeldata.getUsername());
		
		if(model.getPassword().equals(modeldata.getPassword()))
			
				return ResponseEntity.ok(model) ;
		
		   return (ResponseEntity<Model>) ResponseEntity.internalServerError();
	}

//	public List<User> getdata() {
//		// TODO Auto-generated method stub
//		return repo.findAll();
//	}

	public List<Object> gettotal(int id){
		return repo.get(id);
	}

	public int getdata(String username) {
		// TODO Auto-generated method stub
	Model mode=this.repo.findByusername(username);
	int id=0;
	if(mode!=null) {
		id=mode.getId();
		return id ;
	}else
		return 0;
		
	}

	
}

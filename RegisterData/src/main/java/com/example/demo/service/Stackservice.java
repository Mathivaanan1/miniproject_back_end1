package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.User;
import com.example.demo.repository.Stackrepository;

@Service
public class Stackservice {
	
	@Autowired
	Stackrepository rep;

	public List<User> getdata() {
		
		return rep.findAll();
	}

	public List<User> getone(String companyname) {
		
		return this.rep.findBycompanyname(companyname);
	}

	

//	public List<User> getbyCompanyname(String companyname) {
//		// TODO Auto-generated method stub
//		return rep.getbyCompanyname(companyname);
//	}

	

}
